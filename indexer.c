#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <ctype.h>

#define INITIAL_HASH_SIZE 1000
#define LOAD_FACTOR 0.75

// Estrutura para armazenar informa��es sobre uma palavra na tabela hash
typedef struct WordInfo {
    char word[50];
    int count;
    struct FileList *files;
    struct WordInfo *next;
} WordInfo;

// Estrutura para armazenar informa��es sobre a frequ�ncia de uma palavra
typedef struct WordFrequency {
    char word[50];
    int count;
} WordFrequency;

// Estrutura para armazenar a lista de arquivos onde uma palavra ocorre
typedef struct FileList {
    char filename[256];
    struct FileList *next;
} FileList;

// Estrutura para uma entrada na tabela hash
typedef struct HashEntry {
    WordInfo *head;
} HashEntry;

// Estrutura para a tabela hash
typedef struct HashTable {
    HashEntry *entries;
    size_t size;
    size_t count;
} HashTable;

// Fun��o hash simples
unsigned int hashFunction(const char *word) {
    unsigned int hash = 0;
    while (*word) {
        hash = (hash * 31) + tolower(*word++);
    }
    return hash;
}

// Fun��o para converter uma palavra para min�sculas
void toLowerCase(char *str) {
    while (*str) {
        *str = tolower(*str);
        str++;
    }
}

// Fun��o para adicionar uma palavra � tabela hash
void addWord(HashTable *hashTable, const char *word, const char *filename) {
    if (strlen(word) < 2) {
        return; // Ignora palavras com menos de 2 caracteres
    }

    char lowercaseWord[50];
    int j = 0;
    for (int i = 0; word[i] != '\0'; i++) {
        if (isalpha(word[i])) {
            lowercaseWord[j++] = tolower(word[i]);
        }
    }
    lowercaseWord[j] = '\0';

    unsigned int hashValue = hashFunction(lowercaseWord) % hashTable->size;

    WordInfo *current = hashTable->entries[hashValue].head;

    while (current != NULL) {
        if (strcmp(current->word, lowercaseWord) == 0) {
            // A palavra j� existe na tabela, incrementa o contador
            current->count++;
            // Adiciona o arquivo � lista, se ainda n�o estiver presente
            FileList *fileNode = current->files;
            while (fileNode != NULL) {
                if (strcmp(fileNode->filename, filename) == 0) {
                    // O arquivo j� est� na lista
                    return;
                }
                fileNode = fileNode->next;
            }
            // Adiciona o arquivo � lista
            FileList *newFile = (FileList *)malloc(sizeof(FileList));
            if (newFile == NULL) {
                fprintf(stderr, "Erro ao alocar mem�ria para a lista de arquivos\n");
                exit(EXIT_FAILURE);
            }
            strcpy(newFile->filename, filename);
            newFile->next = current->files;
            current->files = newFile;
            return;
        }
        current = current->next;
    }

    // A palavra n�o existe na tabela, adiciona uma nova entrada
    WordInfo *newWord = (WordInfo *)malloc(sizeof(WordInfo));
    if (newWord == NULL) {
        fprintf(stderr, "Erro ao alocar mem�ria para uma nova palavra\n");
        exit(EXIT_FAILURE);
    }

    strcpy(newWord->word, lowercaseWord);
    newWord->count = 1;
    newWord->files = NULL;
    // Adiciona o arquivo � lista
    FileList *newFile = (FileList *)malloc(sizeof(FileList));
    if (newFile == NULL) {
        fprintf(stderr, "Erro ao alocar mem�ria para a lista de arquivos\n");
        exit(EXIT_FAILURE);
    }
    strcpy(newFile->filename, filename);
    newFile->next = NULL;
    newWord->files = newFile;

    newWord->next = hashTable->entries[hashValue].head;
    hashTable->entries[hashValue].head = newWord;

    hashTable->count++;

    // Se o fator de carga for atingido, redimensiona a tabela hash
    if ((double)hashTable->count / hashTable->size > LOAD_FACTOR) {
        resizeHashTable(hashTable, hashTable->size * 2);
    }
}

// Fun��o para redimensionar a tabela hash
void resizeHashTable(HashTable *hashTable, size_t newSize) {
    HashTable newHashTable;
    newHashTable.size = newSize;
    newHashTable.count = 0;
    newHashTable.entries = (HashEntry *)calloc(newSize, sizeof(HashEntry));
    if (newHashTable.entries == NULL) {
        fprintf(stderr, "Erro ao alocar mem�ria para a nova tabela hash\n");
        exit(EXIT_FAILURE);
    }

    // Reinsere as palavras na nova tabela hash
    for (size_t i = 0; i < hashTable->size; i++) {
        WordInfo *current = hashTable->entries[i].head;
        while (current != NULL) {
            FileList *fileNode = current->files;
            while (fileNode != NULL) {
                // Adiciona a palavra na nova tabela hash
                addWord(&newHashTable, current->word, fileNode->filename);
                fileNode = fileNode->next;
            }
            WordInfo *temp = current;
            current = current->next;
            free(temp);
        }
    }

    // Libera a mem�ria da tabela hash antiga
    free(hashTable->entries);

    // Atualiza a tabela hash com a nova tabela redimensionada
    hashTable->size = newHashTable.size;
    hashTable->count = newHashTable.count;
    hashTable->entries = newHashTable.entries;
}

// Fun��o para comparar duas estruturas WordFrequency para ordena��o
int compareWordFrequency(const void *a, const void *b) {
    return ((WordFrequency *)b)->count - ((WordFrequency *)a)->count;
}

// Fun��o para mostrar a frequ�ncia das N palavras mais frequentes
void showTopNWords(HashTable *hashTable, int n) {
    WordFrequency *wordFrequencies = (WordFrequency *)malloc(hashTable->count * sizeof(WordFrequency));
    if (wordFrequencies == NULL) {
        fprintf(stderr, "Erro ao alocar mem�ria para as frequ�ncias das palavras\n");
        exit(EXIT_FAILURE);
    }

    size_t wordFreqIndex = 0;

    // Preenche a estrutura WordFrequency com os dados da tabela hash
    for (size_t i = 0; i < hashTable->size; i++) {
        WordInfo *current = hashTable->entries[i].head;
        while (current != NULL) {
            strcpy(wordFrequencies[wordFreqIndex].word, current->word);
            wordFrequencies[wordFreqIndex].count = current->count;
            wordFreqIndex++;

            WordInfo *temp = current;
            current = current->next;
            free(temp);
        }
    }

    // Ordena a estrutura WordFrequency em ordem decrescente
    qsort(wordFrequencies, hashTable->count, sizeof(WordFrequency), compareWordFrequency);

    // Imprime a frequ�ncia das N palavras mais frequentes
    printf("\nFrequ�ncia das %d palavras mais frequentes:\n", n);
    size_t limit = hashTable->count < n ? hashTable->count : n;
    for (size_t i = 0; i < limit; i++) {
        printf("%s: %d\n", wordFrequencies[i].word, wordFrequencies[i].count);
    }

    // Libera a mem�ria alocada
    free(wordFrequencies);
}

// Fun��o para mostrar a frequ�ncia de uma palavra espec�fica
void showWordFrequency(HashTable *hashTable, const char *word) {
    if (strlen(word) < 2) {
        printf("Palavra deve ter pelo menos 2 caracteres\n");
        return;
    }

    char lowercaseWord[50];
    int j = 0;
    for (int i = 0; word[i] != '\0'; i++) {
        if (isalpha(word[i])) {
            lowercaseWord[j++] = tolower(word[i]);
        }
    }
    lowercaseWord[j] = '\0';

    unsigned int hashValue = hashFunction(lowercaseWord) % hashTable->size;

    WordInfo *current = hashTable->entries[hashValue].head;

    while (current != NULL) {
        if (strcmp(current->word, lowercaseWord) == 0) {
            // A palavra foi encontrada na tabela, imprime a contagem
            printf("Frequ�ncia de '%s': %d\n", word, current->count);

            // Imprime a lista de arquivos onde a palavra ocorre
            FileList *fileNode = current->files;
            printf("Arquivos onde ocorre:\n");
            while (fileNode != NULL) {
                printf("- %s\n", fileNode->filename);
                fileNode = fileNode->next;
            }
            return;
        }
        current = current->next;
    }

    // A palavra n�o foi encontrada na tabela
    printf("Palavra '%s' n�o encontrada\n", word);
}

// Fun��o para mostrar a lista de arquivos mais relevantes para um termo de busca
void showTopNFilesForWord(HashTable *hashTable, const char *word, int n) {
    if (strlen(word) < 2) {
        printf("Termo de busca deve ter pelo menos 2 caracteres\n");
        return;
    }

    char lowercaseWord[50];
    int j = 0;
    for (int i = 0; word[i] != '\0'; i++) {
        if (isalpha(word[i])) {
            lowercaseWord[j++] = tolower(word[i]);
        }
    }
    lowercaseWord[j] = '\0';

    unsigned int hashValue = hashFunction(lowercaseWord) % hashTable->size;

    WordInfo *current = hashTable->entries[hashValue].head;

    // Lista para armazenar informa��es sobre os arquivos e sua relev�ncia
    typedef struct FileInfo {
        char filename[256];
        int relevance;
    } FileInfo;

    FileInfo *fileInfoArray = (FileInfo *)malloc(hashTable->count * sizeof(FileInfo));
    if (fileInfoArray == NULL) {
        fprintf(stderr, "Erro ao alocar mem�ria para as informa��es dos arquivos\n");
        exit(EXIT_FAILURE);
    }

    size_t fileIndex = 0;

    // Preenche a estrutura FileInfo com os dados da tabela hash
    while (current != NULL) {
        if (strcmp(current->word, lowercaseWord) == 0) {
            FileList *fileNode = current->files;
            while (fileNode != NULL) {
                strcpy(fileInfoArray[fileIndex].filename, fileNode->filename);
                fileInfoArray[fileIndex].relevance = current->count;
                fileIndex++;
                fileNode = fileNode->next;
            }
            break;
        }
        current = current->next;
    }

    // Ordena a estrutura FileInfo em ordem decrescente de relev�ncia
    qsort(fileInfoArray, fileIndex, sizeof(FileInfo), compareWordFrequency);

    // Imprime a lista dos N arquivos mais relevantes
    printf("\nArquivos mais relevantes para o termo '%s':\n", word);
    size_t limit = fileIndex < n ? fileIndex : n;
    for (size_t i = 0; i < limit; i++) {
        printf("%s: Relev�ncia %d\n", fileInfoArray[i].filename, fileInfoArray[i].relevance);
    }

    // Libera a mem�ria alocada
    free(fileInfoArray);
}

// Fun��o para percorrer um arquivo, transformar as palavras em min�sculas e contar
void processFile(HashTable *hashTable, const char *filename) {
    FILE *file = fopen(filename, "r");
    if (file == NULL) {
        printf("Erro ao abrir o arquivo %s\n", filename);
        return;
    }

    printf("Processando arquivo: %s\n", filename);

    char word[50];
    while (fscanf(file, "%49s", word) == 1) {
        toLowerCase(word);
        addWord(hashTable, word, filename);
    }

    fclose(file);
}

// Fun��o para percorrer uma pasta e contar as palavras em todos os arquivos
void countWordsInFolder(const char *folderPath) {
    DIR *dir;
    struct dirent *entry;

    // Inicializa a tabela hash
    HashTable hashTable;
    hashTable.size = INITIAL_HASH_SIZE;

    hashTable.count = 0;
    hashTable.entries = (HashEntry *)calloc(hashTable.size, sizeof(HashEntry));
    if (hashTable.entries == NULL) {
        fprintf(stderr, "Erro ao alocar mem�ria para a tabela hash\n");
        exit(EXIT_FAILURE);
    }

    // Abre o diret�rio
    dir = opendir(folderPath);
    if (dir == NULL) {
        fprintf(stderr, "Erro ao abrir o diret�rio %s\n", folderPath);
        free(hashTable.entries);
        exit(EXIT_FAILURE);
    }

    // L� cada entrada no diret�rio
    while ((entry = readdir(dir)) != NULL) {
        // Ignora as entradas "." e ".."
        if (strcmp(entry->d_name, ".") != 0 && strcmp(entry->d_name, "..") != 0) {
            char filePath[256];
            sprintf(filePath, "%s/%s", folderPath, entry->d_name);
            processFile(&hashTable, filePath);
        }
    }

    // Fecha o diret�rio
    closedir(dir);

    // Solicita op��es de entrada ao usu�rio
    int option;
    printf("Escolha uma op��o:\n");
    printf("1. --freq N\n");
    printf("2. --freq-word PALAVRA\n");
    printf("3. --search TERMO\n");
    scanf("%d", &option);

    // Executa a op��o escolhida
    switch (option) {
        case 1:
            {
                int n;
                printf("Digite N (o n�mero de palavras mais frequentes a serem exibidas): ");
                scanf("%d", &n);
                showTopNWords(&hashTable, n);
            }
            break;
        case 2:
            {
                char word[50];
                printf("Digite a PALAVRA: ");
                scanf("%s", word);
                showWordFrequency(&hashTable, word);
            }
            break;
        case 3:
            {
                char term[50];
                int n;
                printf("Digite o TERMO: ");
                scanf("%s", term);
                printf("Digite N (o n�mero de arquivos mais relevantes a serem exibidos): ");
                scanf("%d", &n);
                showTopNFilesForWord(&hashTable, term, n);
            }
            break;
        default:
            printf("Op��o inv�lida\n");
            break;
    }

    // Libera a mem�ria alocada
    free(hashTable.entries);
}

int main() {
    char folderPath[256];

    // Solicita o nome da pasta ao usu�rio
    printf("Digite o nome da pasta: ");
    scanf("%s", folderPath);

    // Conta as palavras na pasta
    countWordsInFolder(folderPath);

    return 0;
}
